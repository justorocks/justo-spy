# justo-spy

A test spy library.

*Proudly made with ♥ in Valencia, Spain, EU.*

For **JavaScript** doc, see `README.js.md`.
