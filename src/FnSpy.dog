#imports
use Calls

#A function spy.
export type FnSpy()
  :calls := Calls()

#Return a call.
fn FnSpy.call(...args) : FnCall = :calls.call(...args)

#Return the last call.
fn FnSpy.lastCall() : FnCall = :calls.lastCall()

#Return the times called.
fn FnSpy.called() : num = :calls.called()

#Return the times called with the given arguments.
fn FnSpy.calledWith(...args) : num = :calls.calledWith(...args)

#Check whether the function was always called with the given arguments.
fn FnSpy.alwaysCalledWith(...args) : bool = :calls.alwaysCalledWith(...args)

#Return the times returning.
fn FnSpy.returned(...args) : num = :calls.returned(...args)

#Check whether the function always returned.
fn FnSpy.alwaysReturned(...args) : bool = :calls.alwaysReturned(...args)

#Return the times raised an error.
fn FnSpy.raised(...args) : num = :calls.raised(...args)

#Check whether always raised an error.
fn FnSpy.alwaysRaised(...args) : bool = :calls.alwaysRaised(...args)
