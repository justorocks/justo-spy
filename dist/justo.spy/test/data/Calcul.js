"use strict";

var _dogmalang = require("dogmalang");

const $Calcul = class Calcul {
  constructor() {
    {
      this.sums = 0;this.muls = 0;
    }
  }
};
const Calcul = new Proxy($Calcul, { apply(receiver, self, args) {
    return new $Calcul(...args);
  } });module.exports = exports = Calcul;
Calcul.prototype.sum = function (x, y) {
  _dogmalang.dogma.paramExpected("x", x, null);_dogmalang.dogma.paramExpected("y", y, null);{
    this.sums += 1;if (_dogmalang.dogma.is(y, _dogmalang.text)) {
      _dogmalang.dogma.raise("invalid op.");
    }return x + y;
  }
};
Calcul.prototype.mul = function (x, y) {
  _dogmalang.dogma.paramExpected("x", x, null);_dogmalang.dogma.paramExpected("y", y, null);{
    this.muls += 1;if (_dogmalang.dogma.is(y, _dogmalang.text)) {
      _dogmalang.dogma.raise("invalid op.");
    }return x * y;
  }
};