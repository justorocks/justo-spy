"use strict";

var _dogmalang = require("dogmalang");

var _justo = require("justo");

const assert = _dogmalang.dogma.use(require("justo.assert"));const spy = _dogmalang.dogma.use(require("../../../justo.spy"));const Calcul = _dogmalang.dogma.use(require("../data/Calcul"));
module.exports = exports = (0, _justo.suite)("method spy", () => {
  {
    (0, _justo.suite)("check private attributes", () => {
      {
        (0, _justo.test)("spy.obj(object, member)", () => {
          {
            let o = spy.obj(Calcul(), "sum()");let s = spy(o);assert(o).isMap();assert(s).isMap();assert(s._fields).eq([]);assert(s._methods).eq(["sum"]);assert(s._calls).has("sum()");
          }
        });(0, _justo.test)("spy.obj(object, members)", () => {
          {
            let o = spy.obj(Calcul(), ["sum()", "mul()"]);let s = spy(o);assert(o).isMap();assert(s).isMap();assert(s._fields).eq([]);assert(s._methods).eq(["sum", "mul"]);assert(s._calls).has(["sum()", "mul()"]);
          }
        });
      }
    });(0, _justo.test)("Call to method spy - always returning", () => {
      {
        let o = spy.obj(Calcul(), "sum()");let s = spy(o);assert(o.sum(1, 2)).eq(3);assert(o.mul(1, 2)).eq(2);assert(o.sum(3, 4)).eq(7);assert(o.mul(3, 4)).eq(12);assert(s.called("sum()")).eq(2);assert(s.calledWith("sum()", [1, 2])).eq(1);assert(s.returned("sum()")).eq(2);assert(s.alwaysReturned("sum()")).eq(true);assert(s.alwaysReturned("sum()", 3)).eq(false);assert(s.raised("sum()")).eq(0);assert(s.alwaysRaised("sum()")).eq(false);assert(s.alwaysRaised("sum()", "err.")).eq(false);assert(s.call("sum()", 0)).has({ ["args"]: [1, 2], ["value"]: 3, ["error"]: null });assert(s.call("sum()", 1)).has({ ["args"]: [3, 4], ["value"]: 7, ["error"]: null });
      }
    });(0, _justo.test)("Call to method spy - always raising", () => {
      {
        let o = spy.obj(Calcul(), "sum()");let s = spy(o);assert(() => {
          {
            o.sum(1, "a");
          }
        }).raises("invalid op.");assert(o.mul(1, 2)).eq(2);assert(() => {
          {
            o.sum(3, "b");
          }
        }).raises("invalid op.");assert(o.mul(3, 4)).eq(12);assert(s.called("sum()")).eq(2);assert(s.returned("sum()")).eq(0);assert(s.alwaysReturned("sum()")).eq(false);assert(s.alwaysReturned("sum()", 3)).eq(false);assert(s.raised("sum()")).eq(2);assert(s.alwaysRaised("sum()")).eq(true);assert(s.alwaysRaised("sum()", "err.")).eq(false);assert(s.call("sum()", 0)).has({ ["args"]: [1, "a"], ["value"]: null, ["error"]: "invalid op." });assert(s.call("sum()", 1)).has({ ["args"]: [3, "b"], ["value"]: null, ["error"]: "invalid op." });
      }
    });(0, _justo.test)("Call to method spy - always raising", () => {
      {
        let o = spy.obj(Calcul(), "sum()");let s = spy(o);assert(() => {
          {
            o.sum(1, "a");
          }
        }).raises("invalid op.");assert(o.mul(1, 2)).eq(2);assert(() => {
          {
            o.sum(3, "b");
          }
        }).raises("invalid op.");assert(o.mul(3, 4)).eq(12);assert(s.called("sum()")).eq(2);assert(s.returned("sum()")).eq(0);assert(s.alwaysReturned("sum()")).eq(false);assert(s.alwaysReturned("sum()", 3)).eq(false);assert(s.raised("sum()")).eq(2);assert(s.alwaysRaised("sum()")).eq(true);assert(s.alwaysRaised("sum()", "err.")).eq(false);assert(s.call("sum()", 0)).has({ ["args"]: [1, "a"], ["value"]: null, ["error"]: "invalid op." });assert(s.call("sum()", 1)).has({ ["args"]: [3, "b"], ["value"]: null, ["error"]: "invalid op." });
      }
    });(0, _justo.suite)("Not being spied", () => {
      {
        let o;let s;(0, _justo.init)("*", () => {
          {
            o = spy.obj(Calcul(), "sum()");s = spy(o);
          }
        }).title("Create spy");(0, _justo.test)("call()", () => {
          {
            assert(() => {
              {
                s.call("unknown()", 0);
              }
            }).raises("'unknown()' is not being spied.");
          }
        });(0, _justo.test)("called()", () => {
          {
            assert(() => {
              {
                s.called("unknown()");
              }
            }).raises("'unknown()' is not being spied.");
          }
        });(0, _justo.test)("returned()", () => {
          {
            assert(() => {
              {
                s.returned("unknown()");
              }
            }).raises("'unknown()' is not being spied.");
          }
        });(0, _justo.test)("alwaysReturned()", () => {
          {
            assert(() => {
              {
                s.alwaysReturned("unknown()");
              }
            }).raises("'unknown()' is not being spied.");
          }
        });(0, _justo.test)("raised()", () => {
          {
            assert(() => {
              {
                s.raised("unknown()");
              }
            }).raises("'unknown()' is not being spied.");
          }
        });(0, _justo.test)("alwaysRaised()", () => {
          {
            assert(() => {
              {
                s.alwaysRaised("unknown()");
              }
            }).raises("'unknown()' is not being spied.");
          }
        });
      }
    });
  }
});