"use strict";

var _dogmalang = require("dogmalang");

var _justo = require("justo");

const assert = _dogmalang.dogma.use(require("justo.assert"));const spy = _dogmalang.dogma.use(require("../../../justo.spy"));
module.exports = exports = (0, _justo.suite)("function spy", () => {
  {
    (0, _justo.test)("spy.func(func) - never called", () => {
      {
        let f = spy.func((x, y) => {
          _dogmalang.dogma.paramExpected("x", x, null);_dogmalang.dogma.paramExpected("y", y, null);{
            return x + y;
          }
        });let s = spy(f);assert(f).isFn();assert(s.called()).eq(0);assert(s.calledWith([1, 2])).eq(0);assert(s.alwaysCalledWith([1, 2])).eq(false);assert(s.raised()).eq(0);assert(s.raised("err.")).eq(0);assert(s.alwaysRaised()).eq(false);assert(s.alwaysRaised("err.")).eq(false);assert(s.returned()).eq(0);assert(s.alwaysReturned()).eq(false);
      }
    });(0, _justo.test)("spy.func()", () => {
      {
        let f = spy.func();let s = spy(f);assert(f).isFn();assert(f(1, 2)).eq(null);assert(f(3, 4)).eq(null);assert(f(3, 4)).eq(null);assert(f(5, 6)).eq(null);assert(s.called()).eq(4);assert(s.calledWith([1, 2])).eq(1);assert(s.calledWith([3, 4])).eq(2);assert(s.calledWith([5, 6])).eq(1);assert(s.calledWith([0, 0])).eq(0);assert(s.alwaysCalledWith([1, 2])).eq(false);
      }
    });(0, _justo.test)("spy.func(fn) - always returned", () => {
      {
        let f = spy.func((x, y) => {
          _dogmalang.dogma.paramExpected("x", x, null);_dogmalang.dogma.paramExpected("y", y, null);{
            return x + y;
          }
        });let s = spy(f);assert(f).isFn();assert(f(1, 2)).eq(3);assert(f(3, 4)).eq(7);assert(f(3, 4)).eq(7);assert(f(5, 6)).eq(11);assert(s.called()).eq(4);assert(s.calledWith([1, 2])).eq(1);assert(s.calledWith([3, 4])).eq(2);assert(s.calledWith([5, 6])).eq(1);assert(s.calledWith([0, 0])).eq(0);assert(s.alwaysCalledWith([1, 2])).eq(false);assert(s.returned()).eq(4);assert(s.returned(7)).eq(2);assert(s.alwaysReturned()).eq(true);assert(s.alwaysReturned(3)).eq(false);assert(s.raised()).eq(0);assert(s.raised("err.")).eq(0);assert(s.alwaysRaised()).eq(false);assert(s.call(0)).has({ ["args"]: [1, 2], ["value"]: 3, ["error"]: null });assert(s.call(1)).has({ ["args"]: [3, 4], ["value"]: 7, ["error"]: null });assert(s.call(2)).has({ ["args"]: [3, 4], ["value"]: 7, ["error"]: null });assert(s.call(3)).has({ ["args"]: [5, 6], ["value"]: 11, ["error"]: null });assert(s.lastCall()).has({ ["args"]: [5, 6], ["value"]: 11, ["error"]: null });
      }
    });(0, _justo.test)("spy.func(fn) - always returned the same", () => {
      {
        let f = spy.func((x, y) => {
          _dogmalang.dogma.paramExpected("x", x, null);_dogmalang.dogma.paramExpected("y", y, null);{
            return x + y;
          }
        });let s = spy(f);assert(f).isFn();assert(f(1, 2)).eq(3);assert(f(2, 1)).eq(3);assert(s.called()).eq(2);assert(s.calledWith([1, 2])).eq(1);assert(s.calledWith([2, 1])).eq(1);assert(s.alwaysCalledWith([1, 2])).eq(false);assert(s.returned()).eq(2);assert(s.returned(3)).eq(2);assert(s.alwaysReturned()).eq(true);assert(s.alwaysReturned(3)).eq(true);assert(s.raised()).eq(0);assert(s.raised("err.")).eq(0);assert(s.alwaysRaised()).eq(false);assert(s.call(0)).has({ ["args"]: [1, 2], ["value"]: 3, ["error"]: null });assert(s.call(1)).has({ ["args"]: [2, 1], ["value"]: 3, ["error"]: null });assert(s.lastCall()).has({ ["args"]: [2, 1], ["value"]: 3, ["error"]: null });
      }
    });(0, _justo.test)("spy.func(fn) - always called with the same", () => {
      {
        let f = spy.func((x, y) => {
          _dogmalang.dogma.paramExpected("x", x, null);_dogmalang.dogma.paramExpected("y", y, null);{
            return x + y;
          }
        });let s = spy(f);assert(f).isFn();assert(f(1, 2)).eq(3);assert(f(1, 2)).eq(3);assert(f(1, 2)).eq(3);assert(s.called()).eq(3);assert(s.calledWith([1, 2])).eq(3);assert(s.alwaysCalledWith([1, 2])).eq(true);assert(s.returned()).eq(3);assert(s.returned(3)).eq(3);assert(s.alwaysReturned()).eq(true);assert(s.alwaysReturned(3)).eq(true);assert(s.raised()).eq(0);assert(s.raised("err.")).eq(0);assert(s.alwaysRaised()).eq(false);assert(s.call(0)).has({ ["args"]: [1, 2], ["value"]: 3, ["error"]: null });assert(s.call(1)).has({ ["args"]: [1, 2], ["value"]: 3, ["error"]: null });assert(s.call(2)).has({ ["args"]: [1, 2], ["value"]: 3, ["error"]: null });
      }
    });(0, _justo.test)("spy.func(fn) - always raised", () => {
      {
        let f = spy.func(msg => {
          _dogmalang.dogma.paramExpected("msg", msg, null);{
            return _dogmalang.dogma.raise(msg);
          }
        });let s = spy(f);assert(f).isFn();assert(() => {
          {
            return f("a");
          }
        }).raises("a");assert(() => {
          {
            return f("b");
          }
        }).raises("b");assert(() => {
          {
            return f("c");
          }
        }).raises("c");assert(s.called()).eq(3);assert(s.calledWith(["a"])).eq(1);assert(s.calledWith(["b"])).eq(1);assert(s.calledWith(["c"])).eq(1);assert(s.alwaysCalledWith([1, "a"])).eq(false);assert(s.returned()).eq(0);assert(s.returned("err.")).eq(0);assert(s.alwaysReturned()).eq(false);assert(s.alwaysReturned(3)).eq(false);assert(s.raised()).eq(3);assert(s.raised("a")).eq(1);assert(s.raised("b")).eq(1);assert(s.raised("c")).eq(1);assert(s.alwaysRaised()).eq(true);assert(s.call(0)).has({ ["args"]: ["a"], ["value"]: null, ["error"]: "a" });assert(s.call(1)).has({ ["args"]: ["b"], ["value"]: null, ["error"]: "b" });assert(s.call(2)).has({ ["args"]: ["c"], ["value"]: null, ["error"]: "c" });
      }
    });(0, _justo.test)("spy.func(fn) - always raised the same", () => {
      {
        let f = spy.func(() => {
          {
            _dogmalang.dogma.raise("err.");
          }
        });let s = spy(f);assert(f).isFn();assert(() => {
          {
            return f("a");
          }
        }).raises("err.");assert(() => {
          {
            return f("b");
          }
        }).raises("err.");assert(() => {
          {
            return f("c");
          }
        }).raises("err.");assert(s.called()).eq(3);assert(s.calledWith(["a"])).eq(1);assert(s.calledWith(["b"])).eq(1);assert(s.calledWith(["c"])).eq(1);assert(s.alwaysCalledWith([1, "a"])).eq(false);assert(s.returned()).eq(0);assert(s.returned("err.")).eq(0);assert(s.alwaysReturned()).eq(false);assert(s.alwaysReturned(3)).eq(false);assert(s.raised()).eq(3);assert(s.raised("err.")).eq(3);assert(s.alwaysRaised()).eq(true);assert(s.alwaysRaised("err.")).eq(true);assert(s.call(0)).has({ ["args"]: ["a"], ["value"]: null, ["error"]: "err." });assert(s.call(1)).has({ ["args"]: ["b"], ["value"]: null, ["error"]: "err." });assert(s.call(2)).has({ ["args"]: ["c"], ["value"]: null, ["error"]: "err." });
      }
    });(0, _justo.suite)("(always)calledWith()", () => {
      {
        (0, _justo.test)("calledWith() - always the same", () => {
          {
            let f = spy.func(x => {
              _dogmalang.dogma.paramExpected("x", x, null);{
                return (0, _dogmalang.text)(x);
              }
            });let s = spy(f);assert(f(123)).eq("123");assert(f(123)).eq("123");assert(s.calledWith(123)).eq(2);assert(s.calledWith([123])).eq(2);assert(s.alwaysCalledWith(123)).eq(true);assert(s.alwaysCalledWith([123])).eq(true);assert(s.calledWith(321)).eq(0);assert(s.calledWith([321])).eq(0);assert(s.alwaysCalledWith(321)).eq(false);assert(s.alwaysCalledWith([321])).eq(false);
          }
        });(0, _justo.test)("calledWith() - always not the same", () => {
          {
            let f = spy.func(x => {
              _dogmalang.dogma.paramExpected("x", x, null);{
                return (0, _dogmalang.text)(x);
              }
            });let s = spy(f);assert(f(123)).eq("123");assert(f(135)).eq("135");assert(s.calledWith(123)).eq(1);assert(s.calledWith([123])).eq(1);assert(s.alwaysCalledWith(123)).eq(false);assert(s.alwaysCalledWith([123])).eq(false);assert(s.calledWith(135)).eq(1);assert(s.calledWith([135])).eq(1);assert(s.alwaysCalledWith(135)).eq(false);assert(s.alwaysCalledWith([135])).eq(false);assert(s.calledWith(321)).eq(0);assert(s.calledWith([321])).eq(0);assert(s.alwaysCalledWith(321)).eq(false);assert(s.alwaysCalledWith([321])).eq(false);
          }
        });
      }
    });
  }
});