"use strict";

var _dogmalang = require("dogmalang");

var _justo = require("justo");

const assert = _dogmalang.dogma.use(require("justo.assert"));const spy = _dogmalang.dogma.use(require("../../../justo.spy"));
module.exports = exports = (0, _justo.suite)("justo.spy", () => {
  {
    (0, _justo.test)("api", () => {
      {
        assert(spy).isFn();assert(spy.func).isFn();assert(spy.obj).isFn();
      }
    });(0, _justo.test)("spy() - not being spied", () => {
      {
        assert(() => {
          {
            spy({});
          }
        }).raises();
      }
    });
  }
});