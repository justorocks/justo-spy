"use strict";

var _dogmalang = require("dogmalang");

var _justo = require("justo");

const assert = _dogmalang.dogma.use(require("justo.assert"));const spy = _dogmalang.dogma.use(require("../../../justo.spy"));
module.exports = exports = (0, _justo.suite)("field spy", () => {
  {
    (0, _justo.test)("spy.obj(obj, mem)", () => {
      {
        let o = spy.obj({ ["f1"]: "v1", ["f2"]: "v2" }, "@f1");let s = spy(o);assert(o).isMap();assert(s).isMap();assert(s._fields).eq(["f1"]);assert(s._methods).eq([]);assert(s._calls).has("@f1");
      }
    });(0, _justo.test)("spy(obj, mems)", () => {
      {
        let o = spy.obj({ ["f1"]: "v1", ["f2"]: "v2" }, ["@f1", "@f3"]);let s = spy(o);assert(o).isMap();assert(s).isMap();assert(s._fields).eq(["f1", "f3"]);assert(s._methods).eq([]);
      }
    });(0, _justo.test)("read", () => {
      {
        let o = spy.obj({ ["f1"]: "v1", ["f2"]: "v2" }, "@f1");let s = spy(o);assert(o.f1).eq("v1");assert(o.f2).eq("v2");assert(o.f1).eq("v1");assert(s.called("@f1")).eq(2);assert(s.returned("@f1")).eq(2);assert(s.returned("@f1", "v1")).eq(2);assert(s.returned("@f1", "v2")).eq(0);assert(s.alwaysReturned("@f1")).eq(true);assert(s.alwaysReturned("@f1", "v1")).eq(true);assert(s.alwaysReturned("@f1", "v2")).eq(false);assert(s.raised("@f1")).eq(0);assert(s.alwaysRaised("@f1")).eq(false);assert(s.alwaysRaised("@f1", "v1")).eq(false);assert(s.call("@f1", 0)).has({ ["accessor"]: "get", ["value"]: "v1", ["error"]: null });assert(s.call("@f1", 1)).has({ ["accessor"]: "get", ["value"]: "v1", ["error"]: null });
      }
    });(0, _justo.test)("write", () => {
      {
        let o = spy.obj({ ["f1"]: "v1", ["f2"]: "v2" }, "@f1");let s = spy(o);o.f1 = "new1.1";o.f2 = "new2.1";o.f1 = "new1.2";assert(s.called("@f1")).eq(2);assert(s.returned("@f1")).eq(2);assert(s.returned("@f1", "new1.1")).eq(1);assert(s.returned("@f1", "new1.2")).eq(1);assert(s.returned("@f1", "v1")).eq(0);assert(s.alwaysReturned("@f1")).eq(true);assert(s.alwaysReturned("@f1", "v1")).eq(false);assert(s.alwaysReturned("@f1", "new1.1")).eq(false);assert(s.alwaysReturned("@f1", "new1.2")).eq(false);assert(s.raised("@f1")).eq(0);assert(s.alwaysRaised("@f1")).eq(false);assert(s.alwaysRaised("@f1", "new1.1")).eq(false);assert(s.call("@f1", 0)).has({ ["accessor"]: "set", ["value"]: "new1.1", ["error"]: null });assert(s.call("@f1", 1)).has({ ["accessor"]: "set", ["value"]: "new1.2", ["error"]: null });
      }
    });(0, _justo.test)("r/w", () => {
      {
        let o = spy.obj({ ["f1"]: "v1", ["f2"]: "v2" }, "@f1");let s = spy(o);assert(o.f1).eq("v1");o.f1 = "new1.1";assert(o.f1).eq("new1.1");o.f2 = "new2.1";assert(o.f2).eq("new2.1");o.f1 = "new1.2";assert(o.f1).eq("new1.2");assert(s.called("@f1")).eq(5);assert(s.returned("@f1")).eq(5);assert(s.returned("@f1", "new1.1")).eq(2);assert(s.returned("@f1", "new1.2")).eq(2);assert(s.returned("@f1", "v1")).eq(1);assert(s.returned("@f1", "unknown")).eq(0);assert(s.alwaysReturned("@f1")).eq(true);assert(s.alwaysReturned("@f1", "v1")).eq(false);assert(s.alwaysReturned("@f1", "new1.1")).eq(false);assert(s.alwaysReturned("@f1", "new1.2")).eq(false);assert(s.alwaysReturned("@f1", "unknown")).eq(false);assert(s.raised("@f1")).eq(0);assert(s.alwaysRaised("@f1")).eq(false);assert(s.alwaysRaised("@f1", "new1.1")).eq(false);assert(s.call("@f1", 0)).has({ ["accessor"]: "get", ["value"]: "v1", ["error"]: null });assert(s.call("@f1", 1)).has({ ["accessor"]: "set", ["value"]: "new1.1", ["error"]: null });assert(s.call("@f1", 2)).has({ ["accessor"]: "get", ["value"]: "new1.1", ["error"]: null });assert(s.call("@f1", 3)).has({ ["accessor"]: "set", ["value"]: "new1.2", ["error"]: null });assert(s.call("@f1", 4)).has({ ["accessor"]: "get", ["value"]: "new1.2", ["error"]: null });
      }
    });(0, _justo.suite)("not being spied", () => {
      {
        let o;let s;(0, _justo.init)("*", () => {
          {
            o = spy.obj({ ["f1"]: "v1", ["f2"]: "v2" }, "@f1");s = spy(o);
          }
        }).title("Create spy");(0, _justo.test)("call()", () => {
          {
            assert(() => {
              {
                s.call("@unknown", 0);
              }
            }).raises("'@unknown' is not being spied.");
          }
        });(0, _justo.test)("called()", () => {
          {
            assert(() => {
              {
                s.called("@unknown");
              }
            }).raises("'@unknown' is not being spied.");
          }
        });(0, _justo.test)("returned()", () => {
          {
            assert(() => {
              {
                s.returned("@unknown");
              }
            }).raises("'@unknown' is not being spied.");
          }
        });(0, _justo.test)("alwaysReturned()", () => {
          {
            assert(() => {
              {
                s.alwaysReturned("@unknown");
              }
            }).raises("'@unknown' is not being spied.");
          }
        });(0, _justo.test)("raised()", () => {
          {
            assert(() => {
              {
                s.raised("@unknown");
              }
            }).raises("'@unknown' is not being spied.");
          }
        });(0, _justo.test)("alwaysRaised()", () => {
          {
            assert(() => {
              {
                s.alwaysRaised("@unknown");
              }
            }).raises("'@unknown' is not being spied.");
          }
        });
      }
    });
  }
});