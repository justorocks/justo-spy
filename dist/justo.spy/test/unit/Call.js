"use strict";

var _dogmalang = require("dogmalang");

var _justo = require("justo");

const assert = _dogmalang.dogma.use(require("justo.assert"));const Call = _dogmalang.dogma.use(require("../../../justo.spy/src/Call"));
module.exports = exports = (0, _justo.suite)("Call", () => {
  {
    (0, _justo.suite)("constructor", () => {
      {
        (0, _justo.test)("constructor(value)", () => {
          {
            assert(Call(123)).has({ ["value"]: 123, ["error"]: null });
          }
        });(0, _justo.test)("constructor(nil, error)", () => {
          {
            assert(Call(null, 123)).has({ ["value"]: null, ["error"]: 123 });
          }
        });
      }
    });
  }
});