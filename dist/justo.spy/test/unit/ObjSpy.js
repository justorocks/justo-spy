"use strict";

var _dogmalang = require("dogmalang");

var _justo = require("justo");

const assert = _dogmalang.dogma.use(require("justo.assert"));const spy = _dogmalang.dogma.use(require("../../../justo.spy"));const Calcul = _dogmalang.dogma.use(require("../data/Calcul"));
module.exports = exports = (0, _justo.suite)("object spy", () => {
  {
    (0, _justo.test)("invalid member format", () => {
      {
        assert(() => {
          {
            spy.obj({}, "member");
          }
        }).raises("invalid member: 'member'. Format: '@field' or 'method()'.");
      }
    });(0, _justo.test)("spying fields and methods", () => {
      {
        let o = spy.obj(Calcul(), ["@sums", "@muls", "sum()", "mul()"]);let s = spy(o);assert(o).isMap().isInstanceOf(Calcul);assert(s).isMap();assert(s._fields).eq(["sums", "muls"]);assert(s._methods).eq(["sum", "mul"]);assert(s._calls).has(["@sums", "@muls", "sum()", "mul()"]);assert(o.sum(1, 2)).eq(3);assert(o.sums).eq(1);assert(o.mul(1, 2)).eq(2);assert(o.muls).eq(1);assert(o.sum(3, 4)).eq(7);assert(o.sums).eq(2);assert(o.mul(3, 4)).eq(12);assert(o.muls).eq(2);assert(s.called("sum()")).eq(2);assert(s.returned("sum()")).eq(2);assert(s.alwaysReturned("sum()")).eq(true);assert(s.alwaysReturned("sum()", 3)).eq(false);assert(s.raised("sum()")).eq(0);assert(s.alwaysRaised("sum()")).eq(false);assert(s.alwaysRaised("sum()", "err.")).eq(false);assert(s.called("mul()")).eq(2);assert(s.returned("mul()")).eq(2);assert(s.alwaysReturned("mul()")).eq(true);assert(s.alwaysReturned("mul()", 3)).eq(false);assert(s.raised("mul()")).eq(0);assert(s.alwaysRaised("mul()")).eq(false);assert(s.alwaysRaised("mul()", "err.")).eq(false);assert(s.called("@sums")).eq(2);assert(s.returned("@sums")).eq(2);assert(s.called("@muls")).eq(2);assert(s.returned("@muls")).eq(2);assert(s.call("sum()", 0)).has({ ["args"]: [1, 2], ["value"]: 3, ["error"]: null });assert(s.call("sum()", 1)).has({ ["args"]: [3, 4], ["value"]: 7, ["error"]: null });assert(s.call("mul()", 0)).has({ ["args"]: [1, 2], ["value"]: 2, ["error"]: null });assert(s.call("mul()", 1)).has({ ["args"]: [3, 4], ["value"]: 12, ["error"]: null });
      }
    });(0, _justo.suite)("alwaysCalledWith()", () => {
      {
        (0, _justo.test)("alwaysCalledWith() : true", () => {
          {
            let o = spy.obj(Calcul(), ["@sums", "@muls", "sum()", "mul()"]);let s = spy(o);assert(o).isMap().isInstanceOf(Calcul);assert(s).isMap();assert(s._fields).eq(["sums", "muls"]);assert(s._methods).eq(["sum", "mul"]);assert(s._calls).has(["@sums", "@muls", "sum()", "mul()"]);assert(o.sum(1, 2)).eq(3);assert(o.sum(1, 2)).eq(3);assert(o.sum(1, 2)).eq(3);assert(o.sum(1, 2)).eq(3);assert(s.alwaysCalledWith("sum()", [1, 2])).eq(true);
          }
        });(0, _justo.test)("alwaysCalledWith() : false", () => {
          {
            let o = spy.obj(Calcul(), ["@sums", "@muls", "sum()", "mul()"]);let s = spy(o);assert(o).isMap().isInstanceOf(Calcul);assert(s).isMap();assert(s._fields).eq(["sums", "muls"]);assert(s._methods).eq(["sum", "mul"]);assert(s._calls).has(["@sums", "@muls", "sum()", "mul()"]);assert(o.sum(1, 2)).eq(3);assert(o.sum(1, 3)).eq(4);assert(o.sum(1, 2)).eq(3);assert(o.sum(1, 2)).eq(3);assert(s.alwaysCalledWith("sum()", [1, 2])).eq(false);
          }
        });(0, _justo.test)("alwaysCalledWith() - not being spied", () => {
          {
            let o = spy.obj(Calcul(), ["@sums", "@muls", "sum()", "mul()"]);let s = spy(o);assert(() => {
              {
                s.alwaysCalledWith("SUM()", "hi");
              }
            }).raises("'SUM()' is not being spied.");
          }
        });
      }
    });(0, _justo.test)("calledWith() - not being spied", () => {
      {
        let o = spy.obj(Calcul(), ["@sums", "@muls", "sum()", "mul()"]);let s = spy(o);assert(() => {
          {
            s.calledWith("SUM()", "hi");
          }
        }).raises("'SUM()' is not being spied.");
      }
    });
  }
});