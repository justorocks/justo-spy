"use strict";

var _dogmalang = require("dogmalang");

const Call = _dogmalang.dogma.use(require("./Call"));
const $FieldCall = class FieldCall extends Call {
  constructor(accessor, val, err) {
    _dogmalang.dogma.paramExpected("accessor", accessor, null);super(val, err);Object.defineProperty(this, "accessor", { value: accessor, enum: true, writable: false });{}
  }
};
const FieldCall = new Proxy($FieldCall, { apply(receiver, self, args) {
    return new $FieldCall(...args);
  } });module.exports = exports = FieldCall;