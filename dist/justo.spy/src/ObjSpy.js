"use strict";

var _dogmalang = require("dogmalang");

const Calls = _dogmalang.dogma.use(require("./Calls"));const Call = _dogmalang.dogma.use(require("./Call"));const FieldCall = _dogmalang.dogma.use(require("./FieldCall"));const MethodCall = _dogmalang.dogma.use(require("./MethodCall"));
const $ObjSpy = class ObjSpy {
  constructor(obj, mems) {
    _dogmalang.dogma.paramExpected("obj", obj, null);_dogmalang.dogma.paramExpected("mems", mems, [_dogmalang.list, _dogmalang.text]);mems = (0, _dogmalang.list)(mems);Object.defineProperty(this, "_obj", { value: obj, writable: false });{
      _dogmalang.dogma.update(this, { name: "fields", visib: ":", assign: ":=", value: [] }, { name: "methods", visib: ":", assign: ":=", value: [] }, { name: "calls", visib: ":", assign: ":=", value: {} });for (let mem of mems) {
        if (_dogmalang.dogma.like(mem, "^@")) {
          _dogmalang.dogma.lshift(this._fields, _dogmalang.dogma.getSlice(mem, 1, -1));
        } else if (_dogmalang.dogma.like(mem, "\\(\\)$")) {
          _dogmalang.dogma.lshift(this._methods, _dogmalang.dogma.getSlice(mem, 0, -3));
        } else {
          _dogmalang.dogma.raise("invalid member: '%s'. Format: '@field' or 'method()'.", mem);
        }_dogmalang.dogma.setItem("=", this._calls, mem, Calls());
      }
    }
  }
};
const ObjSpy = new Proxy($ObjSpy, { apply(receiver, self, args) {
    return new $ObjSpy(...args);
  } });module.exports = exports = ObjSpy;
ObjSpy.prototype._get = function (name) {
  _dogmalang.dogma.paramExpected("name", name, null);{
    if (_dogmalang.dogma.includes(this._fields, name)) {
      let val = _dogmalang.dogma.getItem(this._obj, name);_dogmalang.dogma.getItem(this._calls, "@" + name).insert(FieldCall("get", val, null));return val;
    } else if (_dogmalang.dogma.includes(this._methods, name)) {
      return (...args) => {
        {
          const f = _dogmalang.dogma.getItem(this._obj, name);{
            let [ok, resp] = _dogmalang.dogma.peval(() => {
              return f.apply(this._obj, args);
            });if (ok) {
              _dogmalang.dogma.getItem(this._calls, name + "()").insert(MethodCall(args, resp, null));return resp;
            } else {
              _dogmalang.dogma.getItem(this._calls, name + "()").insert(MethodCall(args, null, resp));_dogmalang.dogma.raise(resp);
            }
          }
        }
      };
    } else {
      return _dogmalang.dogma.getItem(this._obj, name);
    }
  }
};
ObjSpy.prototype._set = function (name, val) {
  _dogmalang.dogma.paramExpected("name", name, null);{
    if (_dogmalang.dogma.includes(this._fields, name)) {
      _dogmalang.dogma.getItem(this._calls, "@" + name).insert(FieldCall("set", val, null));
    }_dogmalang.dogma.setItem("=", this._obj, name, val);
  }
};
ObjSpy.prototype.call = function (mem, i) {
  _dogmalang.dogma.paramExpected("mem", mem, _dogmalang.text);_dogmalang.dogma.paramExpected("i", i, _dogmalang.num);{
    let calls = _dogmalang.dogma.getItem(this._calls, mem);if (!calls) {
      _dogmalang.dogma.raise("'%s' is not being spied.", mem);
    }return calls.call(i);
  }
};
ObjSpy.prototype.called = function (mem) {
  _dogmalang.dogma.paramExpected("mem", mem, _dogmalang.text);{
    let calls = _dogmalang.dogma.getItem(this._calls, mem);if (!calls) {
      _dogmalang.dogma.raise("'%s' is not being spied.", mem);
    }return calls.called();
  }
};
ObjSpy.prototype.calledWith = function (mem, args) {
  _dogmalang.dogma.paramExpected("mem", mem, _dogmalang.text);_dogmalang.dogma.paramExpected("args", args, null);{
    let calls = _dogmalang.dogma.getItem(this._calls, mem);if (!calls) {
      _dogmalang.dogma.raise("'%s' is not being spied.", mem);
    }return calls.calledWith(args);
  }
};
ObjSpy.prototype.alwaysCalledWith = function (mem, args) {
  _dogmalang.dogma.paramExpected("mem", mem, _dogmalang.text);_dogmalang.dogma.paramExpected("args", args, null);{
    let calls = _dogmalang.dogma.getItem(this._calls, mem);if (!calls) {
      _dogmalang.dogma.raise("'%s' is not being spied.", mem);
    }return calls.alwaysCalledWith(args);
  }
};
ObjSpy.prototype.returned = function (mem, ...val) {
  _dogmalang.dogma.paramExpected("mem", mem, _dogmalang.text);{
    let calls = _dogmalang.dogma.getItem(this._calls, mem);if (!calls) {
      _dogmalang.dogma.raise("'%s' is not being spied.", mem);
    }return calls.returned(...val);
  }
};
ObjSpy.prototype.alwaysReturned = function (mem, ...val) {
  _dogmalang.dogma.paramExpected("mem", mem, _dogmalang.text);{
    let calls = _dogmalang.dogma.getItem(this._calls, mem);if (!calls) {
      _dogmalang.dogma.raise("'%s' is not being spied.", mem);
    }return calls.alwaysReturned(...val);
  }
};
ObjSpy.prototype.raised = function (mem, ...val) {
  _dogmalang.dogma.paramExpected("mem", mem, _dogmalang.text);{
    let calls = _dogmalang.dogma.getItem(this._calls, mem);if (!calls) {
      _dogmalang.dogma.raise("'%s' is not being spied.", mem);
    }return calls.raised(...val);
  }
};
ObjSpy.prototype.alwaysRaised = function (mem, ...val) {
  _dogmalang.dogma.paramExpected("mem", mem, _dogmalang.text);{
    let calls = _dogmalang.dogma.getItem(this._calls, mem);if (!calls) {
      _dogmalang.dogma.raise("'%s' is not being spied.", mem);
    }return calls.alwaysRaised(...val);
  }
};