"use strict";

var _dogmalang = require("dogmalang");

const ObjSpy = _dogmalang.dogma.use(require("./ObjSpy"));const MethodCall = _dogmalang.dogma.use(require("./MethodCall"));const FieldCall = _dogmalang.dogma.use(require("./FieldCall"));
function ObjProxy(obj, mems) {
  _dogmalang.dogma.paramExpected("obj", obj, null);_dogmalang.dogma.paramExpected("mems", mems, null);{
    Object.defineProperty(obj, "_spy", { value: ObjSpy(obj, mems) });return (0, _dogmalang.proxy)(obj, { ["get"]: (tgt, prop) => {
        _dogmalang.dogma.paramExpected("tgt", tgt, null);_dogmalang.dogma.paramExpected("prop", prop, null);{
          return tgt._spy._get(prop);
        }
      }, ["set"]: (tgt, prop, val) => {
        _dogmalang.dogma.paramExpected("tgt", tgt, null);_dogmalang.dogma.paramExpected("prop", prop, null);{
          tgt._spy._set(prop, val);return true;
        }
      } });
  }
}module.exports = exports = ObjProxy;