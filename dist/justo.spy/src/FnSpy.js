"use strict";

var _dogmalang = require("dogmalang");

const Calls = _dogmalang.dogma.use(require("./Calls"));
const $FnSpy = class FnSpy {
  constructor() {
    {
      Object.defineProperty(this, "_calls", { value: Calls() });
    }
  }
};
const FnSpy = new Proxy($FnSpy, { apply(receiver, self, args) {
    return new $FnSpy(...args);
  } });module.exports = exports = FnSpy;
FnSpy.prototype.call = function (...args) {
  {
    return this._calls.call(...args);
  }
};
FnSpy.prototype.lastCall = function () {
  {
    return this._calls.lastCall();
  }
};
FnSpy.prototype.called = function () {
  {
    return this._calls.called();
  }
};
FnSpy.prototype.calledWith = function (...args) {
  {
    return this._calls.calledWith(...args);
  }
};
FnSpy.prototype.alwaysCalledWith = function (...args) {
  {
    return this._calls.alwaysCalledWith(...args);
  }
};
FnSpy.prototype.returned = function (...args) {
  {
    return this._calls.returned(...args);
  }
};
FnSpy.prototype.alwaysReturned = function (...args) {
  {
    return this._calls.alwaysReturned(...args);
  }
};
FnSpy.prototype.raised = function (...args) {
  {
    return this._calls.raised(...args);
  }
};
FnSpy.prototype.alwaysRaised = function (...args) {
  {
    return this._calls.alwaysRaised(...args);
  }
};