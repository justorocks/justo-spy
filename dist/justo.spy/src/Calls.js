"use strict";

var _dogmalang = require("dogmalang");

const Call = _dogmalang.dogma.use(require("./Call"));const FnCall = _dogmalang.dogma.use(require("./FnCall"));
const $Calls = class Calls {
  constructor() {
    {
      this._items = [];
    }
  }
};
const Calls = new Proxy($Calls, { apply(receiver, self, args) {
    return new $Calls(...args);
  } });module.exports = exports = Calls;
Calls.prototype.insert = function (call) {
  _dogmalang.dogma.paramExpected("call", call, Call);{
    return _dogmalang.dogma.lshift(this._items, call);
  }
};
Calls.prototype.call = function (i) {
  _dogmalang.dogma.paramExpected("i", i, _dogmalang.num);{
    return _dogmalang.dogma.getItem(this._items, i);
  }
};
Calls.prototype.lastCall = function () {
  {
    return _dogmalang.dogma.getItem(this._items, -1);
  }
};
Calls.prototype.called = function () {
  {
    return (0, _dogmalang.len)(this._items);
  }
};
Calls.prototype.calledWith = function (args) {
  let times = 0;_dogmalang.dogma.paramExpected("args", args, null);{
    for (let call of this._items) {
      if (call.calledWith(args)) {
        times += 1;
      }
    }
  }return times;
};
Calls.prototype.alwaysCalledWith = function (args) {
  let resp = false;_dogmalang.dogma.paramExpected("args", args, null);{
    if ((0, _dogmalang.len)(this._items) != 0) {
      resp = this.called() == this.calledWith(args);
    }
  }return resp;
};
Calls.prototype.returned = function (...args) {
  let times = 0;{
    if ((0, _dogmalang.len)(this._items) > 0) {
      if ((0, _dogmalang.len)(args) == 0) {
        for (let call of this._items) {
          if (call.returned()) {
            times += 1;
          }
        }
      } else {
        for (let call of this._items) {
          if (call.returned(_dogmalang.dogma.getItem(args, 0))) {
            times += 1;
          }
        }
      }
    }
  }return times;
};
Calls.prototype.alwaysReturned = function (...args) {
  {
    let times;if ((0, _dogmalang.len)(this._items) > 0) {
      times = 0;if ((0, _dogmalang.len)(args) == 0) {
        for (let call of this._items) {
          if (call.returned()) {
            times += 1;
          }
        }
      } else {
        for (let call of this._items) {
          if (call.returned(_dogmalang.dogma.getItem(args, 0))) {
            times += 1;
          }
        }
      }
    }return times == (0, _dogmalang.len)(this._items);
  }
};
Calls.prototype.raised = function (...args) {
  let times = 0;{
    if ((0, _dogmalang.len)(args) == 0) {
      for (let call of this._items) {
        if (call.raised()) {
          times += 1;
        }
      }
    } else {
      for (let call of this._items) {
        if (call.raised(_dogmalang.dogma.getItem(args, 0))) {
          times += 1;
        }
      }
    }
  }return times;
};
Calls.prototype.alwaysRaised = function (...args) {
  {
    let times;if ((0, _dogmalang.len)(this._items) > 0) {
      times = 0;if ((0, _dogmalang.len)(args) == 0) {
        for (let call of this._items) {
          if (call.raised()) {
            times += 1;
          }
        }
      } else {
        for (let call of this._items) {
          if (call.raised(_dogmalang.dogma.getItem(args, 0))) {
            times += 1;
          }
        }
      }
    }return times == (0, _dogmalang.len)(this._items);
  }
};