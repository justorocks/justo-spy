"use strict";

var _dogmalang = require("dogmalang");

const assert = _dogmalang.dogma.use(require("assert"));const Call = _dogmalang.dogma.use(require("./Call"));
const $FnCall = class FnCall extends Call {
  constructor(args, val, err) {
    _dogmalang.dogma.paramExpected("args", args, null);super(val, err);Object.defineProperty(this, "args", { value: args, enum: true, writable: false });{}
  }
};
const FnCall = new Proxy($FnCall, { apply(receiver, self, args) {
    return new $FnCall(...args);
  } });module.exports = exports = FnCall;
FnCall.prototype.calledWith = function (args) {
  _dogmalang.dogma.paramExpected("args", args, null);{
    if (_dogmalang.dogma.isNot(args, _dogmalang.list)) {
      args = [args];
    }return _dogmalang.dogma.getItem(_dogmalang.dogma.peval(() => {
      return assert.deepEqual(this.args, args);
    }), 0);
  }
};