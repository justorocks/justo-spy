"use strict";

var _dogmalang = require("dogmalang");

const FnSpy = _dogmalang.dogma.use(require("./FnSpy"));const FnCall = _dogmalang.dogma.use(require("./FnCall"));
function FnProxy(func) {
  {
    Object.defineProperty(func, "_spy", { value: FnSpy() });return (0, _dogmalang.proxy)(func, { ["apply"]: (tgt, this_, args) => {
        _dogmalang.dogma.paramExpected("tgt", tgt, null);_dogmalang.dogma.paramExpected("args", args, null);{
          {
            let [ok, resp] = _dogmalang.dogma.peval(() => {
              return tgt(...args);
            });if (ok) {
              tgt._spy._calls.insert(FnCall(args, resp, null));return resp;
            } else {
              tgt._spy._calls.insert(FnCall(args, null, resp));_dogmalang.dogma.raise(resp);
            }
          }
        }
      } });
  }
}module.exports = exports = FnProxy;