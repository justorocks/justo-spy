"use strict";

var _dogmalang = require("dogmalang");

const assert = _dogmalang.dogma.use(require("assert"));
const $Call = class Call {
  constructor(value, error) {
    Object.defineProperty(this, "value", { value: value, enum: true, writable: false });Object.defineProperty(this, "error", { value: error, enum: true, writable: false });{}
  }
};
const Call = new Proxy($Call, { apply(receiver, self, args) {
    return new $Call(...args);
  } });module.exports = exports = Call;
Call.prototype.returned = function (val) {
  let resp = false;{
    if (this.error) {
      resp = false;
    } else {
      if (val == null) {
        resp = true;
      } else {
        resp = _dogmalang.dogma.getItem(_dogmalang.dogma.peval(() => {
          return assert.deepEqual(this.value, val);
        }), 0);
      }
    }
  }return resp;
};
Call.prototype.raised = function (err) {
  let resp = false;{
    if (!this.error) {
      resp = false;
    } else {
      if (err) {
        resp = this.error == err;
      } else {
        resp = true;
      }
    }
  }return resp;
};