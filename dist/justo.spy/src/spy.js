"use strict";

var _dogmalang = require("dogmalang");

const FnProxy = _dogmalang.dogma.use(require("./FnProxy"));const ObjProxy = _dogmalang.dogma.use(require("./ObjProxy"));
function spy(proxy) {
  let resp;_dogmalang.dogma.paramExpected("proxy", proxy, null);{
    resp = proxy._spy;if (!resp) {
      _dogmalang.dogma.raise("'%s' is not being spied.", proxy);
    }
  }return resp;
}module.exports = exports = spy;
spy.func = (...args) => {
  let resp;{
    if ((0, _dogmalang.len)(args) == 0) {
      args = [() => {
        {}
      }];
    }resp = FnProxy(...args);
  }return resp;
};
spy.obj = (...args) => {
  {
    return ObjProxy(...args);
  }
};