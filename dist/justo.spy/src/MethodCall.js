"use strict";

var _dogmalang = require("dogmalang");

const FnCall = _dogmalang.dogma.use(require("./FnCall"));
const $MethodCall = class MethodCall extends FnCall {
  constructor(args, val, err) {
    super(args, val, err);{}
  }
};
const MethodCall = new Proxy($MethodCall, { apply(receiver, self, args) {
    return new $MethodCall(...args);
  } });module.exports = exports = MethodCall;